﻿namespace DarziApp
{
    partial class PrintPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintPage));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbl_size = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_desc = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.lbl_order = new System.Windows.Forms.Label();
            this.lbl_qty = new System.Windows.Forms.Label();
            this.lbl_ph = new System.Windows.Forms.Label();
            this.lbl_date = new System.Windows.Forms.Label();
            this.lbl_tpay = new System.Windows.Forms.Label();
            this.lbl_ddate = new System.Windows.Forms.Label();
            this.lbl_adv = new System.Windows.Forms.Label();
            this.lbl_advc = new System.Windows.Forms.Label();
            this.lbl_ddatec = new System.Windows.Forms.Label();
            this.lbl_tpayc = new System.Windows.Forms.Label();
            this.lbl_datec = new System.Windows.Forms.Label();
            this.lbl_phc = new System.Windows.Forms.Label();
            this.lbl_qtyc = new System.Windows.Forms.Label();
            this.lbl_orderc = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.lbl_namec = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbl_size);
            this.groupBox2.Location = new System.Drawing.Point(15, 153);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(321, 265);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Size";
            // 
            // lbl_size
            // 
            this.lbl_size.AutoSize = true;
            this.lbl_size.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_size.Location = new System.Drawing.Point(6, 17);
            this.lbl_size.Name = "lbl_size";
            this.lbl_size.Size = new System.Drawing.Size(32, 29);
            this.lbl_size.TabIndex = 8;
            this.lbl_size.Text = "L:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(297, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 26);
            this.label3.TabIndex = 8;
            this.label3.Text = "Shop# UG-5 First Floor Khaleej Bara\r\n  Market Lasbella Chowk Karachi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(656, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 52);
            this.label4.TabIndex = 8;
            this.label4.Text = "Iftikhar Ahmed\r\n0321-2763391\r\n0315-0294191\r\nWhatsapp 0340-8064445";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Order#:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(213, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Suit Qty:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(454, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Date:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(658, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Due Date:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 111);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Name:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(213, 111);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Phone:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(658, 111);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Advance:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(454, 111);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Total Payment:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbl_desc);
            this.groupBox1.Location = new System.Drawing.Point(378, 153);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(418, 265);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Descripton";
            // 
            // lbl_desc
            // 
            this.lbl_desc.AutoSize = true;
            this.lbl_desc.Location = new System.Drawing.Point(6, 17);
            this.lbl_desc.Name = "lbl_desc";
            this.lbl_desc.Size = new System.Drawing.Size(16, 13);
            this.lbl_desc.TabIndex = 9;
            this.lbl_desc.Text = "L:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(654, 476);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(128, 52);
            this.label28.TabIndex = 12;
            this.label28.Text = "Iftikhar Ahmed\r\n0321-2763391\r\n0315-0294191\r\nWhatsapp 0340-8064445\r\n";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(306, 501);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(178, 26);
            this.label29.TabIndex = 13;
            this.label29.Text = "Shop# UG-5 First Floor Khaleej Bara\r\n  Market Lasbella Chowk Karachi\r\n";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(12, 438);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(790, 13);
            this.label34.TabIndex = 18;
            this.label34.Text = resources.GetString("label34.Text");
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(12, 631);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(239, 13);
            this.label39.TabIndex = 21;
            this.label39.Text = "Reciever Signature:  ______________________";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(616, 631);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(178, 13);
            this.label40.TabIndex = 17;
            this.label40.Text = "Master Iftikhar Ahmed Ejaz Ahmed   ";
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Location = new System.Drawing.Point(61, 111);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(38, 13);
            this.lbl_name.TabIndex = 8;
            this.lbl_name.Text = "Name:";
            // 
            // lbl_order
            // 
            this.lbl_order.AutoSize = true;
            this.lbl_order.Location = new System.Drawing.Point(61, 82);
            this.lbl_order.Name = "lbl_order";
            this.lbl_order.Size = new System.Drawing.Size(38, 13);
            this.lbl_order.TabIndex = 23;
            this.lbl_order.Text = "Name:";
            // 
            // lbl_qty
            // 
            this.lbl_qty.AutoSize = true;
            this.lbl_qty.Location = new System.Drawing.Point(266, 82);
            this.lbl_qty.Name = "lbl_qty";
            this.lbl_qty.Size = new System.Drawing.Size(38, 13);
            this.lbl_qty.TabIndex = 24;
            this.lbl_qty.Text = "Name:";
            // 
            // lbl_ph
            // 
            this.lbl_ph.AutoSize = true;
            this.lbl_ph.Location = new System.Drawing.Point(266, 111);
            this.lbl_ph.Name = "lbl_ph";
            this.lbl_ph.Size = new System.Drawing.Size(38, 13);
            this.lbl_ph.TabIndex = 25;
            this.lbl_ph.Text = "Name:";
            // 
            // lbl_date
            // 
            this.lbl_date.AutoSize = true;
            this.lbl_date.Location = new System.Drawing.Point(494, 82);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(38, 13);
            this.lbl_date.TabIndex = 26;
            this.lbl_date.Text = "Name:";
            // 
            // lbl_tpay
            // 
            this.lbl_tpay.AutoSize = true;
            this.lbl_tpay.Location = new System.Drawing.Point(538, 111);
            this.lbl_tpay.Name = "lbl_tpay";
            this.lbl_tpay.Size = new System.Drawing.Size(38, 13);
            this.lbl_tpay.TabIndex = 27;
            this.lbl_tpay.Text = "Name:";
            // 
            // lbl_ddate
            // 
            this.lbl_ddate.AutoSize = true;
            this.lbl_ddate.Location = new System.Drawing.Point(716, 82);
            this.lbl_ddate.Name = "lbl_ddate";
            this.lbl_ddate.Size = new System.Drawing.Size(38, 13);
            this.lbl_ddate.TabIndex = 28;
            this.lbl_ddate.Text = "Name:";
            // 
            // lbl_adv
            // 
            this.lbl_adv.AutoSize = true;
            this.lbl_adv.Location = new System.Drawing.Point(716, 111);
            this.lbl_adv.Name = "lbl_adv";
            this.lbl_adv.Size = new System.Drawing.Size(38, 13);
            this.lbl_adv.TabIndex = 29;
            this.lbl_adv.Text = "Name:";
            // 
            // lbl_advc
            // 
            this.lbl_advc.AutoSize = true;
            this.lbl_advc.Location = new System.Drawing.Point(714, 579);
            this.lbl_advc.Name = "lbl_advc";
            this.lbl_advc.Size = new System.Drawing.Size(38, 13);
            this.lbl_advc.TabIndex = 45;
            this.lbl_advc.Text = "Name:";
            // 
            // lbl_ddatec
            // 
            this.lbl_ddatec.AutoSize = true;
            this.lbl_ddatec.Location = new System.Drawing.Point(714, 550);
            this.lbl_ddatec.Name = "lbl_ddatec";
            this.lbl_ddatec.Size = new System.Drawing.Size(38, 13);
            this.lbl_ddatec.TabIndex = 44;
            this.lbl_ddatec.Text = "Name:";
            // 
            // lbl_tpayc
            // 
            this.lbl_tpayc.AutoSize = true;
            this.lbl_tpayc.Location = new System.Drawing.Point(538, 579);
            this.lbl_tpayc.Name = "lbl_tpayc";
            this.lbl_tpayc.Size = new System.Drawing.Size(38, 13);
            this.lbl_tpayc.TabIndex = 43;
            this.lbl_tpayc.Text = "Name:";
            // 
            // lbl_datec
            // 
            this.lbl_datec.AutoSize = true;
            this.lbl_datec.Location = new System.Drawing.Point(494, 550);
            this.lbl_datec.Name = "lbl_datec";
            this.lbl_datec.Size = new System.Drawing.Size(38, 13);
            this.lbl_datec.TabIndex = 42;
            this.lbl_datec.Text = "Name:";
            // 
            // lbl_phc
            // 
            this.lbl_phc.AutoSize = true;
            this.lbl_phc.Location = new System.Drawing.Point(266, 579);
            this.lbl_phc.Name = "lbl_phc";
            this.lbl_phc.Size = new System.Drawing.Size(38, 13);
            this.lbl_phc.TabIndex = 41;
            this.lbl_phc.Text = "Name:";
            // 
            // lbl_qtyc
            // 
            this.lbl_qtyc.AutoSize = true;
            this.lbl_qtyc.Location = new System.Drawing.Point(266, 550);
            this.lbl_qtyc.Name = "lbl_qtyc";
            this.lbl_qtyc.Size = new System.Drawing.Size(38, 13);
            this.lbl_qtyc.TabIndex = 40;
            this.lbl_qtyc.Text = "Name:";
            // 
            // lbl_orderc
            // 
            this.lbl_orderc.AutoSize = true;
            this.lbl_orderc.Location = new System.Drawing.Point(61, 550);
            this.lbl_orderc.Name = "lbl_orderc";
            this.lbl_orderc.Size = new System.Drawing.Size(38, 13);
            this.lbl_orderc.TabIndex = 39;
            this.lbl_orderc.Text = "Name:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(454, 579);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(78, 13);
            this.label41.TabIndex = 30;
            this.label41.Text = "Total Payment:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(656, 550);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(56, 13);
            this.label42.TabIndex = 31;
            this.label42.Text = "Due Date:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(656, 579);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(53, 13);
            this.label43.TabIndex = 32;
            this.label43.Text = "Advance:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(454, 550);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(33, 13);
            this.label44.TabIndex = 33;
            this.label44.Text = "Date:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(213, 579);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(41, 13);
            this.label45.TabIndex = 34;
            this.label45.Text = "Phone:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(213, 550);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(47, 13);
            this.label46.TabIndex = 35;
            this.label46.Text = "Suit Qty:";
            // 
            // lbl_namec
            // 
            this.lbl_namec.AutoSize = true;
            this.lbl_namec.Location = new System.Drawing.Point(61, 579);
            this.lbl_namec.Name = "lbl_namec";
            this.lbl_namec.Size = new System.Drawing.Size(38, 13);
            this.lbl_namec.TabIndex = 36;
            this.lbl_namec.Text = "Name:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(12, 579);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(38, 13);
            this.label48.TabIndex = 37;
            this.label48.Text = "Name:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(12, 550);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(43, 13);
            this.label49.TabIndex = 38;
            this.label49.Text = "Order#:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(400, 654);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(381, 68);
            this.label1.TabIndex = 44;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(70, 41);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(105, 15);
            this.label13.TabIndex = 8;
            this.label13.Text = "Tanoli Brothers";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(80, 509);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 15);
            this.label14.TabIndex = 8;
            this.label14.Text = "Tanoli Brothers";
            // 
            // btnPrint
            // 
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.Location = new System.Drawing.Point(9, 1);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(65, 60);
            this.btnPrint.TabIndex = 46;
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(15, 468);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 60);
            this.button1.TabIndex = 47;
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            this.printPreviewDialog1.Load += new System.EventHandler(this.printPreviewDialog1_Load);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(24, 684);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 23);
            this.button2.TabIndex = 48;
            this.button2.Text = "Confirm Order";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(308, 458);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(164, 37);
            this.label15.TabIndex = 8;
            this.label15.Text = "ID Tailors";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(116, 684);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 49;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(308, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 37);
            this.label2.TabIndex = 50;
            this.label2.Text = "ID Tailors";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(80, 452);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "Customer Copy";
            // 
            // PrintPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(821, 728);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.lbl_advc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_ddatec);
            this.Controls.Add(this.lbl_tpayc);
            this.Controls.Add(this.lbl_datec);
            this.Controls.Add(this.lbl_phc);
            this.Controls.Add(this.lbl_qtyc);
            this.Controls.Add(this.lbl_orderc);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.lbl_namec);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.lbl_adv);
            this.Controls.Add(this.lbl_ddate);
            this.Controls.Add(this.lbl_tpay);
            this.Controls.Add(this.lbl_date);
            this.Controls.Add(this.lbl_ph);
            this.Controls.Add(this.lbl_qty);
            this.Controls.Add(this.lbl_order);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PrintPage";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.PrintPage_Load);
            this.Shown += new System.EventHandler(this.PrintPage_Shown);
            this.Validated += new System.EventHandler(this.PrintPage_Validated);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbl_size;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Label lbl_order;
        private System.Windows.Forms.Label lbl_qty;
        private System.Windows.Forms.Label lbl_ph;
        private System.Windows.Forms.Label lbl_date;
        private System.Windows.Forms.Label lbl_tpay;
        private System.Windows.Forms.Label lbl_ddate;
        private System.Windows.Forms.Label lbl_adv;
        private System.Windows.Forms.Label lbl_advc;
        private System.Windows.Forms.Label lbl_ddatec;
        private System.Windows.Forms.Label lbl_tpayc;
        private System.Windows.Forms.Label lbl_datec;
        private System.Windows.Forms.Label lbl_phc;
        private System.Windows.Forms.Label lbl_qtyc;
        private System.Windows.Forms.Label lbl_orderc;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label lbl_namec;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label lbl_desc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button button1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label16;
    }
}