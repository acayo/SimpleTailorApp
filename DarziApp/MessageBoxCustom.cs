﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DarziApp
{
    public partial class MessageBoxCustom : Form
    {
        string id;
        public MessageBoxCustom(string id)
        {
            this.id = id;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MessageBox_Load(object sender, EventArgs e)
        {

        }

        private void print_Click(object sender, EventArgs e)
        {
            new PrintPage(true).ShowDialog();
            this.Close();
        }

        private void getsize_Click(object sender, EventArgs e)
        {

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void delete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do You Really Want To Delete This Order??","Tell Me",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                SqlConnection con = new SqlConnection(StaticValuesHolder.CONNECTIONQUERY);
                try
                {
                    con.Close();
                    con.Open();
                    int queryresult = (new SqlCommand(String.Format("DELETE FROM orders WHERE id={0};", id), con)).ExecuteNonQuery();
                    if (queryresult > 0)
                    {
                        MessageBox.Show("Order Deleted Successfuly", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                    }
                }
                catch(Exception)
                {
                    MessageBox.Show("Error Please Restart Program","Critical Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                finally
                {
                    con.Close();
                }
                
            }
        }
    }
}
