﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DarziApp
{
    public partial class CheckOrder : Form
    {
        // GetLastOrderNumber: SELECT TOP 1 ordernumber FROM orders ORDER BY ID DESC;


        SqlConnection con = new SqlConnection(StaticValuesHolder.CONNECTIONQUERY);

        public CheckOrder()
        {
            InitializeComponent();
        }

        private void CheckOrder_Load(object sender, EventArgs e)
        {


           //try
            //{

            //    // 20161212098989
            //    con.Open();
            //    SqlDataReader result = (new SqlCommand("SELECT TOP 1 ordernumber FROM orders ORDER BY ID DESC;"
            //    , con)).ExecuteReader();
            //    if(result.Read() && result.HasRows)
            //    {
            //         StaticValuesHolder.ORDERNUMBER = (Int32.Parse(result.GetValue(0).ToString()).ToString());
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            //finally
            //{
            //    con.Close();
            //}
            
            
            string desc, size;
            desc = StaticValuesHolder.DESCRIPTION;
            desc.Replace("/ ", "\n");

            size = StaticValuesHolder.SIZE;
            size.Replace("/ ", "\n");

            lbl_Name.Text = StaticValuesHolder.NAME;
            lbl_Phone.Text = StaticValuesHolder.PHONE;
            lbl_Size.Text = size.Replace("/ ", "\n");
            lbl_Desc.Text = desc.Replace("/ ", "\n");
            lbl_Suit.Text = StaticValuesHolder.SUITQUANTITY;
            lbl_payment.Text = StaticValuesHolder.TOTALPAYMENT;
            lbl_adv.Text = StaticValuesHolder.ADVANCEPAYMENT;
            lbl_date.Text = StaticValuesHolder.ORDERDATE;
            lbl_ddate.Text = StaticValuesHolder.DUEDATE;
            lbl_orderNumber.Text = StaticValuesHolder.ORDERNUMBER;
        }

        private void btn_confirm_Click(object sender, EventArgs e)
        {



            try
            {
                con.Close();
                con.Open();
                int result = (new SqlCommand(StaticValuesHolder.GetPreparedQuery(), con)).ExecuteNonQuery();
                if (result>0)
                {
                    if (MessageBox.Show("Order Has Been Placed, Do You Want To Print Receipt?", "Success Full Entry", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        (new PrintPage(false)).ShowDialog();

                    }                    

                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void CheckOrder_Activated(object sender, EventArgs e)
        {
            //try
            //{
            //    con.Close();
            //    con.Open();
            //    SqlDataReader result = (new SqlCommand("SELECT TOP 1 ordernumber FROM orders ORDER BY ID DESC;"
            //    , con)).ExecuteReader();
            //    if (result.Read())
            //    {
            //        StaticValuesHolder.ORDERNUMBER = ((Int32.Parse(result.GetValue(0).ToString()).ToString()));
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            //finally
            //{
            //    con.Close();
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                this.Cursor = Cursors.WaitCursor;

                PrintForm pf = new PrintForm(this);
                pf.Print();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                PrintForm pf = new PrintForm(this);
                pf.Print();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
    }
}
